# Commerce License Content Documentation

## About

This module provides Commerce License Type plugin submodules
for licensed content management.

Contains three submodules:
- **Commerce License Publish Content**
  For the licensed management of audience access to content.

- **Commerce License Media Disk Quota**
  For the licensed management of disk quota on Media file storage.

- **Commerce License Membership Content**
  For the licensed management of membership roles, audience access to
  content and disk quota on Media file storage.

This module provides base dependencies for the submodules.
- Product and Variation Type: Automated content license.
- Order and Order Item Type: Commerce license content.
- Checkout flow: Commerce license content.
- View: Commerce License Cart block View.
- View: Commerce License Content Cart form.
- View: Commerce License Content Checkout Order Summary.
- Uninstallation Form.


## Table of contents

- Requirements
- Recommended modules
- Installation
- Uninstallation
- Maintainers
- Sponsors
- Donations


## Requirements

- [User Disk Quota](https://www.drupal.org/project/user_disk_quota):
  Media Disk Quota and Membership Licenses require this module.


## Recommended modules

[Markdown filter](https://www.drupal.org/project/markdown): When enabled,
display of the project's README.md help will be rendered with markdown.


## Installation

1. Install Commerce.

   Enable dev version of inline_entity_form
   to satisfy Drupal Commerce dependency:
       composer require drupal/inline_entity_form @RC

   Ensure the bcmath module dependency for Drupal Commerce is installed on
   your php server.

   Install Drupal Commerce:
       composer require drupal/commerce

   Enable dev version of advancedqueue to satisfy Commerce license dependency:
       composer require drupal/advancedqueue @RC

2. Install Commerce license:
       composer require drupal/commerce_license:2.x-dev

3. Install this module:
       composer require drupal/commerce_license_content


## Configuration

1. Create a default Commerce Store if you haven't already.
   See: /store/add.

2. Create a Commerce Payment Gateway if you haven't already.
   See: /admin/commerce/config/payment-gateways.

3. Enable one or more submodules:
       drush en commerce_licence_publish_content
       drush en commerce_license_media_disk_quota
       drush en commerce_license_membership_content

   Follow the instructions provided in each module README.md.


## Uninstallation

For the entity uninstallation Form,
see: /admin/commerce/config/commerce_license_content_delete.

Uninstalling this module will remove all submodules,
Fields, Licenses, Views, and Products.

    drush pmu commerce_license_content


## Maintainers 

- Dan Greenman - Freelance developer for hire.
  [dangreenman] (https://www.drupal.org/u/dangreenman).

Commission your wildest Open Source software dreams.
See: [Portfolio] (https://greenman-it.pw)


## Sponsors

[Metro Rentals] (http://www.metro-rentals.com/)

Thank you for your kind donation.


## Donations

If you value this module and would like to show your appreciation and support,
please consider sending a donation here:

- [Monero] (44KTVNFzSmC12srxKgxvCEbmUXSzjNmT1NAHRpF9tuhvCDMpsimTZerAxPr4pNrtT7EjqN45WKerrAh1K9UgKayR9ogksYm)

Thank you to the Drupal community for all your fantastic work.
