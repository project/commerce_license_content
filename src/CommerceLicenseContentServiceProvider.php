<?php

namespace Drupal\commerce_license_content;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the commerce_license.multiple_license_order_processor service.
 *
 * Allows multiple license order-items of the same license type in the cart.
 */
class CommerceLicenseContentServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {

    // Overrides commerce license multiple_license_order_processor class
    // to allow multiple licenses in cart.
    if ($container->hasDefinition('commerce_license.multiple_license_order_processor')) {
      $definition = $container->getDefinition('commerce_license.multiple_license_order_processor');
      $definition->setClass('Drupal\commerce_license_content\LicenseOrderProcessorMultiples');
    }
  }

}
