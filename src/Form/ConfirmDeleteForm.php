<?php

namespace Drupal\commerce_license_content\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the uninstallation configuration for Commerce License Content.
 */
class ConfirmDeleteForm extends ConfirmFormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type bundle info interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleTypeInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The form description.
   *
   * @var string
   */
  protected $description;

  /**
   * Constructs a new ConfirmDeleteForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_type_info
   *   The entity type bundle info interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeBundleInfoInterface $bundle_type_info,
    EntityTypeManagerInterface $entity_manager,
    StateInterface $state
  ) {

    $this->messenger = $messenger;
    $this->bundleTypeInfo = $bundle_type_info;
    $this->entityTypeManager = $entity_manager;
    $this->state = $state;
    $this->description = $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the services required to construct this class.
      $container->get('messenger'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_license_content.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // No content flag. Is set to false when installed content is found.
    $no_content = TRUE;

    $form['warning'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>It is safe to ignore this page.</p><strong>Warning:</strong> these are destructive operations intended for uninstallation and clean up purposes. Performing a database backup is recommended before proceeding.'),
    ];

    $node_bundles = $this->bundleTypeInfo->getBundleInfo('node');

    $licensed_node_bundles = [];
    foreach ($node_bundles as $node_bundle => $label) {

      $field = FieldConfig::loadByName(
        'node', $node_bundle, 'field_publish_content_license');

      if ($field) {

        $licensed_node_bundles[$node_bundle] = $label['label'];
      }
    }

    if (count($licensed_node_bundles) > 0) {

      $no_content = FALSE;
      $form['node'] = [
        '#type' => 'details',
        '#title' => $this->t('Remove License field from Content'),
        '#open' => FALSE,
        '#tree' => TRUE,
        '#group' => 'additional_settings',
      ];

      foreach ($licensed_node_bundles as $licensed_node_bundle => $label) {

        $form['node']['remove_field'][$licensed_node_bundle] = [
          '#type' => 'checkbox',
          '#title' => $this->t(
            'Remove License field from %label', ['%label' => $label]),
          '#default_value' => FALSE,
        ];
      }
    }

    $media_bundles = $this->bundleTypeInfo->getBundleInfo('media');

    $licensed_media_bundles = [];
    foreach ($media_bundles as $media_bundle => $label) {

      $field = FieldConfig::loadByName(
        'media', $media_bundle, 'field_media_disk_quota_license');

      if ($field) {

        $licensed_media_bundles[$media_bundle] = $label['label'];
      }
    }

    if (count($licensed_media_bundles) > 0) {

      $no_content = FALSE;
      $form['media'] = [
        '#type' => 'details',
        '#title' => $this->t('Remove License field from Media'),
        '#open' => FALSE,
        '#tree' => TRUE,
        '#group' => 'additional_settings',
      ];

      foreach ($licensed_media_bundles as $licensed_media_bundle => $label) {

        $form['media']['remove_field'][$licensed_media_bundle] = [
          '#type' => 'checkbox',
          '#title' => $this->t(
            'Remove License field from %label', ['%label' => $label]
          ),
          '#default_value' => FALSE,
        ];
      }
    }

    $license_bundles = $this->bundleTypeInfo->getBundleInfo('commerce_license');

    $commerce_licenses = [
      'publish_single',
      'publish_unlimited',
      'media_disk_quota',
      'publish_membership',
    ];
    $installed_licenses = [];
    foreach ($license_bundles as $license_bundle => $label) {

      if (in_array($license_bundle, $commerce_licenses)) {

        $installed_licenses[$license_bundle] = $label['label'];
      }
    }
    if (count($installed_licenses) > 0) {

      $no_content = FALSE;
      // Licenses.
      $form['commerce_license'] = [
        '#type' => 'details',
        '#title' => $this->t('Delete Licenses'),
        '#open' => FALSE,
        '#tree' => TRUE,
        '#group' => 'additional_settings',
      ];
      foreach ($installed_licenses as $license => $label) {

        $form['commerce_license']['uninstall'][$license] = [
          '#type' => 'checkbox',
          '#title' => $this->t(
            "Delete all %label Licenses", ['%label' => $label]),
          '#default_value' => FALSE,
        ];
      }
    }

    $product_bundles = $this->bundleTypeInfo->getBundleInfo('commerce_product');

    $license_products = [
      'media_disk_quota_license',
      'publish_single_license_template',
      'publish_unlimited_license',
      'publish_license_membership',
    ];
    $installed_products = [];
    foreach ($product_bundles as $product_bundle => $label) {

      if (in_array($product_bundle, $license_products)) {

        $installed_products[$product_bundle] = $label['label'];
      }
    }

    if (count($installed_products) > 0) {

      $no_content = FALSE;
      // Commerce products.
      $form['commerce_product_variation'] = [
        '#type' => 'details',
        '#title' => $this->t('Delete Commerce Product Variations'),
        '#open' => FALSE,
        '#tree' => TRUE,
        '#group' => 'additional_settings',
      ];
      foreach ($installed_products as $installed_product => $label) {

        $form['commerce_product_variation']['uninstall'][$installed_product] = [
          '#type' => 'checkbox',
          '#title' => $this->t(
          'Delete all %label Product Variations', ['%label' => $label]),
          '#default_value' => FALSE,
        ];
      }
    }

    $module_name = 'Commerce License Content';

    $form['commerce_order_item'] = [
      '#type' => 'details',
      '#title' => $this->t('Delete Commerce Order Items'),
      '#description' => $this->t('These options will delete order transaction records, which you may need to retain for accounting and legal purposes.'),
      '#open' => FALSE,
      '#tree' => TRUE,
      '#group' => 'additional_settings',
    ];

    $form['commerce_order_item']['uninstall']['commerce_license_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete all %name Order Items', [
        '%name' => $module_name,
      ]),
      '#default_value' => FALSE,
    ];

    $form['commerce_order_item']['remove_type']['commerce_order_item_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete the %name Order Item Type', [
        '%name' => $module_name,
      ]),
      '#default_value' => FALSE,
    ];

    $form['commerce_order_item']['remove_type']['commerce_order_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete the %name Order Type', [
        '%name' => $module_name,
      ]),
      '#default_value' => FALSE,
    ];

    $form['commerce_order_item']['remove_type']['commerce_checkout_flow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete the %name Checkout flow', [
        '%name' => $module_name,
      ]),
      '#default_value' => FALSE,
    ];

    $form['confirmation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Confirm that you understand what you are doing.'),
      '#default_value' => FALSE,
    ];

    if ($no_content === TRUE) {

      // Clear form.
      $form_attributes['#attributes'] = $form['#attributes'];
      $form = $form_attributes;

      // Change form description.
      $this->description = $this->t('No content found to delete.');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    if ($values['confirmation'] == 1) {

      $uninstall = [];
      $remove = [];
      $remove_type = [];
      foreach ($values as $entity_type => $value) {
        if (is_array($value)) {

          if (isset($value['uninstall'])) {
            $uninstall[$entity_type] = $value['uninstall'];
          }

          if (isset($value['remove_field'])) {
            $remove[$entity_type] = $value['remove_field'];
          }

          if (isset($value['remove_type'])) {
            $remove_type[$entity_type] = $value['remove_type'];
          }
        }
      }

      // Uninstall entities.
      foreach ($uninstall as $entity => $bundle_list) {

        $bundle_info = $this->bundleTypeInfo->getBundleInfo($entity);

        $storage = $this->entityTypeManager->getStorage($entity);
        foreach ($bundle_list as $bundle => $checked) {

          if ($checked == 1) {

            $query = $storage->getQuery()
              ->accessCheck(FALSE)
              ->condition('type', $bundle);
            $entity_ids = $query->execute();
            $entity_list = $storage->loadMultiple($entity_ids);

            $deleted_count = 0;
            foreach ($entity_list as $entity) {

              $entity->delete();
              $deleted_count += 1;
            }

            $label = $bundle_info[$bundle]['label'];
            $this->messenger->addMessage(
              $this->t("@count %bundle @entity records deleted.", [
                '@count' => $deleted_count,
                '%bundle' => $label,
                '@entity' => $entity,
              ])
            );
          }
        }
      }

      // Remove License field from selected entity types.
      foreach ($remove as $entity_type => $bundle_list) {

        $field_id = FALSE;
        if ($entity_type == 'node') {
          $field_id = 'field_publish_content_license';
        }

        if ($entity_type == 'media') {
          $field_id = 'field_media_disk_quota_license';
        }

        $bundle_info = $this->bundleTypeInfo->getBundleInfo($entity_type);

        $storage = $this->entityTypeManager->getStorage($entity_type);
        foreach ($bundle_list as $bundle => $checked) {

          if ($checked == 1) {

            // Delete License field from entity.
            $field_instance = '';
            if ($field_id) {

              $field_instance =
                FieldConfig::loadByName($entity_type, $bundle, $field_id);
            }

            if ($field_instance) {

              $field_instance->delete();

              $label = $bundle_info[$bundle]['label'];
              $this->messenger->addMessage(
                $this->t("License field removed from %bundle @entity.", [
                  '%bundle' => $label,
                  '@entity' => $entity_type,
                ])
              );
            }

            // Prepare a list of settings for removing Drupal state keys.
            $settings = [
              'products',
              'order_field',
              'publish_control',
              'access_control',
              'queue_control',
            ];

            // Remove Drupal state keys on bundle.
            foreach ($settings as $setting) {
              $key = 'commerce_license_publish_content_' . $setting . '_' . $bundle;
              $this->state->delete($key);
            }
          }
        }
      }

      // Delete entity types.
      foreach ($remove_type as $entity_type_list) {

        foreach ($entity_type_list as $entity_type => $checked) {

          if ($checked == 1) {

            $entity_storage = $this->entityTypeManager->getStorage($entity_type);
            $entity_query = $entity_storage->getQuery()
              ->accessCheck(FALSE)
              ->condition('id', 'commerce_license_content');
            $entity_ids = $entity_query->execute();
            $entities = $entity_storage->loadMultiple($entity_ids);

            $delete_count = 0;
            foreach ($entities as $entity) {

              $delete_count += 1;
              $entity->delete();
            }
            $this->messenger->addMessage(
              $this->t("Deleted @count Commerce License Content %entity_type.", [
                '@count' => $delete_count,
                '%entity_type' => $entity_type,
              ])
            );
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('commerce_license.configuration');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Commerce License Content uninstall');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

}
