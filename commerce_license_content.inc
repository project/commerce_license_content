<?php

/**
 * @file
 * Provides functions for the Commerce License Content module.
 */

use Drupal\field\Entity\FieldConfig;

/**
 * Updates the 'field_licensed_entity' field on License Products.
 *
 * Enables all node bundles as targets.
 */
function commerce_license_content_update_product_variation_target_field() {

  // Build an array of target node bundles with License fields.
  $node_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('node');

  $target_bundles = [];
  foreach (array_keys($node_bundles) as $node_bundle) {

    $target_bundles[$node_bundle] = $node_bundle;
  }

  // Load Automated License PV field.
  $fields[] = FieldConfig::loadByName('commerce_product_variation', 'automated_content_license', 'field_licensed_entity');

  // Load Single License PV field.
  $fields[] = FieldConfig::loadByName('commerce_product_variation', 'publish_single_license_template', 'field_licensed_entity');

  foreach ($fields as $field) {

    if ($field instanceof FieldConfig) {

      // Build settings array.
      $settings = [
        'handler' => 'default:node',
        'handler_settings' => [
          'target_bundles' => $target_bundles,
          'sort' => [
            'field' => '_none',
            'direction' => 'ASC',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => key($target_bundles),
        ],
      ];

      // Save new settings.
      $field->set('settings', $settings);
      $field->save();
    }
  }
}

/**
 * Deletes a Field from an Entity Type.
 *
 * @param array $entity_type
 *   Entity bundle to delete field from.
 */
function commerce_license_content_remove_field($entity_type, $entity_bundle, $field_name) {

  // Load field.
  $field_instance = FieldConfig::loadByName($entity_type,
    $entity_bundle, $field_name);

  if ($field_instance instanceof FieldConfig) {

    $field_instance->delete();
  }
}

/**
 * Batch deletes a list of entities.
 *
 * @param array $entity_list
 *   A structured array of entity types mapped to filter-field and value.
 */
function commerce_license_content_remove_entities($entity_list) {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  foreach ($entity_list as $entity_type => $filter) {

    foreach ($filter as $field => $value) {

      $storage = $entity_manager->getStorage($entity_type);
      $query = $storage->getQuery()
        ->accessCheck(FALSE)
        ->condition($field, $value);
      $entity_ids = $query->execute();
      if ($entity_ids) {

        $entity_list = $storage->loadMultiple($entity_ids);
        foreach ($entity_list as $entity) {

          $entity->delete();
        }
      }
    }
  }
}
