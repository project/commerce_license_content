<?php

/**
 * @file
 * Provides media hooks for the Commerce Pay to Publish module.
 */

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;

/**
 * Returns active and pending media disk quota license id's for user.
 *
 * @param int $user_id
 *   Optional id to filter results by. Defaults to current user.
 *
 * @return array
 *   Array of license id's.
 */
function commerce_license_media_disk_quota_get_license_ids($user_id = '') {

  if (!$user_id) {
    $account = \Drupal::currentUser();
    $user_id = $account->id();
  }

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $license_ids = [];
  $query = $entity_manager->getStorage('commerce_license')->getQuery()
    ->accessCheck(TRUE)
    ->condition('state', ['active', 'renewal_in_progress', 'pending'], 'IN')
    ->condition('type', ['media_disk_quota', 'publish_membership'], 'IN')
    ->condition('uid', $user_id);

  $license_ids = $query->execute();

  return $license_ids;
}

/**
 * Returns active License data statistics for current user.
 *
 * @param \Drupal\Core\Session\AccountInterface|null $account
 *   Optional User account to get quota statistics for. Defaults to current.
 *
 * @return array
 *   Array of license id's.
 */
function commerce_license_media_disk_quota_get_user_license_data($account) {

  $license_ids = commerce_license_media_disk_quota_get_license_ids($account);

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $license_items = $entity_manager->getStorage('commerce_license')
    ->loadMultiple($license_ids);

  $data['total']['used'] = '0B';
  $data['total']['quota'] = '0B';
  $data['total']['used_raw'] = 0;
  $data['total']['quota_raw'] = 0;
  $data['total']['file_count'] = 0;
  foreach ($license_items as $id => $license) {

    $data[$id] = $license->getTypePlugin()
      ->getLicenseQuotaData($license);

    $data['total']['used_raw'] += $data[$id]['used_raw'];
    $data['total']['quota_raw'] += $data[$id]['amount_raw'];
    $data['total']['file_count'] += $data[$id]['target_entity_count'];
  }

  $data['total']['used'] =
    commerce_license_media_disk_quota_format_filesize($data['total']['used_raw']);
  $data['total']['quota'] =
    commerce_license_media_disk_quota_format_filesize($data['total']['quota_raw']);

  return $data;
}

/**
 * Get active or pending license given a product variation id.
 *
 * @param int $variation_id
 *   The product variation id.
 *
 * @return \Drupal\commerce_license\Entity\LicenseInterface|false
 *   An existing license entity. FALSE otherwise.
 */
function commerce_license_media_disk_quota_get_product_license($variation_id) {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $query = $entity_manager->getStorage('commerce_license')->getQuery()
    ->accessCheck(TRUE)
    ->condition('state', ['pending', 'active', 'renewal_in_progress'], 'IN')
    ->condition('product_variation', $variation_id);

  $license_ids = $query->execute();

  if (!empty($license_ids)) {

    $license_id = array_shift($license_ids);
    $license = $entity_manager->getStorage('commerce_license')
      ->load($license_id);

    return $license;
  }

  return FALSE;
}

/**
 * Returns cart license order items for user.
 *
 * @return array
 *   Returns array of order items, or FALSE.
 */
function commerce_license_media_disk_quota_get_license_cart_items() {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $order_items = [];

  $query = $entity_manager->getStorage('commerce_order_item')->getQuery()
    ->accessCheck(TRUE)
    ->condition('purchased_entity.entity:commerce_product_variation.type',
      ['media_disk_quota_license', 'membership_content_license'], 'IN')
    ->condition('type', 'commerce_license_content')
    ->condition('order_id.entity.state', 'draft');

  $order_item_ids = $query->execute();

  /** @var \Drupal\commerce_order\Entity\OrderItemStorage $order_items */
  $order_items = $entity_manager->getStorage('commerce_order_item')
    ->loadMultiple($order_item_ids);

  return $order_items;
}

/**
 * Get all node id's with field value referencing a license id.
 *
 * @param int $license_id
 *   License id.
 *
 * @return array
 *   Array of node ids.
 */
function commerce_license_media_disk_quota_get_licensed_media_ids($license_id) {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $media_ids = [];
  $query = $entity_manager->getStorage('media')->getQuery()
    ->accessCheck(TRUE)->condition('field_media_disk_quota_license', $license_id);

  $media_ids = $query->execute();

  return $media_ids;
}

/**
 * Get all licensed Product Variation ids.
 *
 * @return array
 *   Array of product names and prices keyed by product ID.
 */
function commerce_license_media_disk_quota_get_license_product_variation_ids() {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  $product_variation_ids = [];

  $query = $entity_manager->getStorage('commerce_product_variation')->getQuery()
    ->accessCheck(TRUE)->condition('type', [
      'media_disk_quota_license', 'membership_content_license',
    ], 'IN');

  $product_variation_ids = $query->execute();

  return $product_variation_ids;
}

/**
 * Returns active/pending licenses available for user to publish Media.
 *
 * @param int $user_id
 *   Optional user id to filter results by. Defaults to current user.
 *
 * @return array
 *   Returns an array of licenses.
 */
function commerce_license_media_disk_quota_get_available_licenses($user_id = '') {

  $license_ids = commerce_license_media_disk_quota_get_license_ids($user_id);

  $licenses = [];
  foreach ($license_ids as $license_id) {

    /** @var \Drupal\commerce_license\Entity\LicenseInterface $license */
    $license = \Drupal::service('entity_type.manager')
      ->getStorage('commerce_license')->load($license_id);

    if ($license) {

      $licenses[$license_id] = $license;
    }
  }

  return $licenses;
}

/**
 * Fetch Media Disk Quota enabled licensed products set on a Media Type.
 *
 * @param string $bundle
 *   Media Entity Type machine name.
 *
 * @return array
 *   An array of product IDs that are enabled for the given entity type.
 */
function commerce_license_media_disk_quota_get_media_type_enabled_products($bundle) {

  $list_options = [];
  if (!empty($bundle)) {

    $key = 'commerce_license_media_disk_quota_products_' . $bundle;
    $enabled_products = \Drupal::state()->get($key, []);

    foreach ($enabled_products as $id => $product) {

      if (!empty($product)) {

        $list_options[$id] = $product;
      }
    }
  }

  return $list_options;
}

/**
 * Generate a price list for Media Disk Quota products set on an Entity Type.
 *
 * @param string $entity_bundle
 *   An optional bundle type to filter the list by.
 *
 * @return array
 *   Array of product names and prices keyed by product ID.
 */
function commerce_license_media_disk_quota_get_product_variations($entity_bundle = '') {

  $variations = [];
  // Get Product Variation options enabled on Media Type.
  $enabled_products =
    commerce_license_media_disk_quota_get_media_type_enabled_products($entity_bundle);

  // Get all license Product Variation ids.
  $product_variation_ids =
    commerce_license_media_disk_quota_get_license_product_variation_ids();

  // Load Product Variations.
  /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variations */
  $product_variations = \Drupal::service('entity_type.manager')
    ->getStorage('commerce_product_variation')->loadMultiple($product_variation_ids);

  foreach ($product_variations as $id => $variation) {

    // If no bundle parameter is specified,
    // add all Media Disk Quota product options to the list.
    if (!$entity_bundle) {

      $variations[$id] = $variation;
    }
    elseif (isset($enabled_products[$id])) {

      // If this product is enabled on bundle, add it to the list.
      $variations[$id] = $variation;
    }
  }

  return $variations;
}

/**
 * Return a price field as a formatted currency string.
 *
 * @param string $price
 *   A price Field Type array, containing a number and currency code.
 * @param string $prefix
 *   Prefix string.
 * @param string $postfix
 *   Postfix string.
 *
 * @return string
 *   Formatted price.
 */
function commerce_license_media_disk_quota_format_price($price, $prefix = NULL, $postfix = NULL) {

  $currency_formatter = \Drupal::service('commerce_price.currency_formatter');
  $number = $price[0]['number'];
  $currency_code = $price[0]['currency_code'];

  return $prefix . $currency_formatter->format($number, $currency_code) . $postfix;
}

/**
 * Generate a price list of license Order Item options for Media Type.
 *
 * @param string $entity_bundle
 *   An optional entity bundle type to filter the list by.
 *
 * @return array
 *   Array of product names and prices keyed by product ID.
 */
function commerce_license_media_disk_quota_build_product_option_list($entity_bundle = '') {

  $options = [];
  // Get a list of available product variations set on bundle.
  $variations =
    commerce_license_media_disk_quota_get_product_variations($entity_bundle);

  foreach ($variations as $id => $variation) {

    $sku = $variation->get('sku')->value;
    $title = $variation->getTitle();
    $price = $variation->get('price')->getValue();
    $options[$sku]['id'] = $id;
    $options[$sku]['product'] =
      commerce_license_media_disk_quota_format_price($price, '', ": $title");
  }
  // Sort list by sku.
  ksort($options);

  // Translate sorted options into product price options list.
  $list = [];
  foreach ($options as $sku => $option) {

    $id = $option['id'];
    $list[$id] = $option['product'];
  }

  return $list;
}

/**
 * Returns an array of license status fields.
 *
 * @param \Drupal\commerce_license\Entity\LicenseInterface $license
 *   License to return status of.
 * @param string $bundle
 *   Bundle to warn of grace period when pending payment.
 *
 * @return array
 *   Array of fields.
 */
function commerce_license_media_disk_quota_build_license_status_fields($license, $bundle) {

  $license_state = $license->getState()->value;
  $product_variation = $license->getPurchasedEntity();
  $license_label = $product_variation->label();

  $fields['license'] = [
    '#type' => 'markup',
    '#markup' => $license_label,
    '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-license'>\n"
    . "<div class='field__label'>" . t('License') . "</div>\n",
    '#suffix' => "</div>\n",
  ];

  $state = ucfirst($license_state);
  $state = preg_replace('|_|', ' ', $state);
  $fields['state'] = [
    '#type' => 'markup',
    '#markup' => $state,
    '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-state'>\n"
    . "<div class='field__label'>" . t('License state') . "</div>\n",
    '#suffix' => "</div>\n",
  ];

  // Get order URL.
  $order = $license->getOriginatingOrder();
  $order_state = $order->state->value;

  if ($order && $order_state != 'draft') {

    $order_url = Url::fromRoute(
      'entity.commerce_order.user_view', [
        'user' => $order->uid->entity->id(),
        'commerce_order' => $order->order_id->value,
      ]
    )->toString();
  }
  else {

    $order_url = Url::fromRoute('commerce_cart.page')->toString();
  }

  $order_markup = '';
  switch ($license_state) {

    case 'pending':

      // Fetch delete action settings.
      $key = 'commerce_license_media_disk_quota_delete_action_' . $bundle;
      $action = \Drupal::state()->get($key, []);

      if ($action == 'delete' || $action == 'change_uid') {

        // Fetch grace period settings.
        $key = 'commerce_license_media_disk_quota_grace_period_' . $bundle;
        $grace = \Drupal::state()->get($key, []);
        $interval = $grace['interval'];
        $period = $grace['period'];

        // Correct grammar.
        if ($interval > 1) {

          $period = $period . "s";
        }
        $grace_interval = "$interval $period";

        $order_markup = t("Pending payment (<a href='@url'>View order</a>)<br><br>License will be activated upon full receipt of payment.<br>You have @grace to complete payment before this media will expire.",
        ['@url' => $order_url, '@grace' => $grace_interval]);
      }
      else {

        $order_markup = t("Pending payment (<a href='@url'>View order</a>).<br>License will be activated upon full receipt of payment.",
        ['@url' => $order_url]);
      }
      break;

    case 'renewal_in_progress':

      $order_markup = t("Renewal in progress. <a href='@url'>View your order</a>.<br>License will be renewed upon full receipt of payment.",
        ['@url' => $order_url]);

    case 'expired':
    case 'active':

      /** @var \Drupal\Core\Datetime\DateFormatterInterface $granted_markup */
      $granted_markup = \Drupal::service('date.formatter')->format(
        $license->getGrantedTime(), 'long');

      // Indicate license grant date.
      $fields['granted'] = [
        '#type' => 'markup',
        '#markup' => $granted_markup,
        '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-granted'>\n"
        . "<div class='field__label'>" . t('License activated') . "</div>\n",
        '#suffix' => "</div>\n",
      ];

      $expires_time = $license->getExpiresTime();

      /** @var \Drupal\Component\Datetime\TimeInterface $time */
      $time = \Drupal::service('datetime.time');
      $now = $time->getRequestTime();
      // Correct grammer of field label relative to time.
      if ($now >= $expires_time) {

        $field_label = t('License expired');
      }
      else {
        $field_label = t('License expires');
      }

      $expires_markup = '';
      if ($expires_time == 0) {

        $expires_markup = 'Unlimited';
      }
      else {

        /** @var \Drupal\Core\Datetime\DateFormatterInterface $expires_markup */
        $expires_markup = \Drupal::service('date.formatter')
          ->format($expires_time, 'long');
      }

      // Indicate license expiry date.
      $fields['expires'] = [
        '#type' => 'markup',
        '#markup' => $expires_markup,
        '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-expires'>\n"
        . "<div class='field__label'>$field_label</div>\n",
        '#suffix' => "</div>\n",
      ];

      break;
  }

  $license_id = $license->id();

  $target_media_ids =
  commerce_license_media_disk_quota_get_licensed_media_ids($license_id);

  $target_count = count($target_media_ids);

  if ($target_count > 0) {

    // Ensure a view exists to show the license summary.
    try {

      // Link page count to the view summarizing targeted content.
      $view_url = Url::fromRoute(
        'view.commerce_license_media_disk_quota.storage', ['arg_0' => $license_id]
      )->toString();

      $count_markup = t('@count, <a href=":url">View</a>',
        [':url' => $view_url, '@count' => $target_count]
      );
    }
    catch (\Exception $e) {

      $count_markup = t('@count', ['@count' => $target_count]);
    }

    // Indicate number of licensed pages.
    $fields['items'] = [
      '#type' => 'markup',
      '#markup' => $count_markup,
      '#prefix' => "<div class='field field--label-inline field--commerce-license-disk-media-quota-items'>\n"
      . "<div class='field__label'>" . t('Licensed media') . "</div>\n",
      '#suffix' => "</div>\n",
    ];
  }

  if (!empty($order_markup)) {

    $fields['order_status'] = [
      '#type' => 'markup',
      '#markup' => $order_markup,
      '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-order-state'>\n"
      . "<div class='field__label'>" . t('Order status') . "</div>\n",
      '#suffix' => "</div>\n",
    ];
  }
  else {
    if ($license->canRenew()) {

      $product_variation = $license->getPurchasedEntity();
      $product_variation_id = $product_variation->id();

      $fields['commerce_license_media_disk_quota_renew_id'] = [
        '#type' => 'hidden',
        '#default_value' => $product_variation_id,
      ];

      // Add a renew license checkbox.
      $fields['commerce_license_media_disk_quota_renew'] = [
        '#type' => 'checkbox',
        '#title' => t('Renew this license'),
        '#default_value' => 0,
        '#prefix' => "<div class='field field--label-inline field--commerce-license-media-disk-quota-order-renew'>\n",
        '#suffix' => "</div>\n",
        '#attributes' => ['id' => 'renew'],
        '#states' => [
          'visible' => [':input[id="order_form"]' => ['checked' => FALSE]],
        ],
      ];
    }
  }

  return $fields;
}

/**
 * Build an options list of Order Item price items available for User on Media.
 *
 * @param int $user_id
 *   Optional user id to filter results by. Defaults to current user.
 *
 * @return array
 *   Array of product names and prices keyed by product ID.
 */
function commerce_license_media_disk_quota_build_license_option_list($user_id = '') {

  $options = [];
  $available_licenses
    = commerce_license_media_disk_quota_get_available_licenses($user_id);

  foreach ($available_licenses as $id => $license) {

    $expires_time = $license->getExpiresTime();
    $license_state = $license->getState()->value;

    if ($license_state == 'active') {

      if ($expires_time != 0) {

        /** @var \Drupal\Core\Datetime\DateFormatterInterface $expires_time */
        $expires_time = \Drupal::service('date.formatter')
          ->format($expires_time, 'short');
        $state = "(Expires: $expires_time)";

      }
      else {

        $state = "(Expires: never)";
      }
    }
    else {

      $state = ucfirst($license_state);
      $state = preg_replace('|_|', ' ', $state);
      $state = "($state)";
    }

    $options[$id] = $license->label() . " $state";
  }

  return $options;
}

/**
 * Add Product Variation to cart.
 *
 * @param \Drupal\commerce_product\ProductVariationInterface $product_variation
 *   The Publish to Pay product variation chosen by the user.
 *
 * @return object
 *   License generated from order_item.
 */
function commerce_license_media_disk_quota_add_to_cart($product_variation) {

  /** @var \Drupal\commerce_cart\CartManagerInterface $cart_manager */
  $cart_manager = \Drupal::service('commerce_cart.cart_manager');

  /** @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface $order_type_resolver */
  $order_type_resolver =
    \Drupal::service('commerce_order.chain_order_type_resolver');

  /** @var \Drupal\commerce_store\CurrentStoreInterface $current_store */
  $current_store = \Drupal::service('commerce_store.current_store');

  /** @var \Drupal\commerce_cart\CartProviderInterface $cart_provider */
  $cart_provider = \Drupal::service('commerce_cart.cart_provider');

  /** @var \Drupal\commerce_order\Entity\OrderItemStorage $order_item_storage */
  $order_item_storage =
    \Drupal::service('entity_type.manager')->getStorage('commerce_order_item');

  /** @var LicenseStorageInterface $license_storage */
  $license_storage =
    \Drupal::service('entity_type.manager')->getStorage('commerce_license');

  // Prepare order item.
  $order_item = $order_item_storage
    ->createFromPurchasableEntity($product_variation, ['quantity' => 1]);

  // Prepare cart.
  $order_type_id = $order_type_resolver->resolve($order_item);
  $store = $current_store->getStore();
  $cart = $cart_provider->getCart($order_type_id, $store);
  if (!$cart) {

    $cart = $cart_provider->createCart($order_type_id, $store);
  }

  // Get existing license.
  $product_variation_id = $product_variation->id();
  $license =
    commerce_license_media_disk_quota_get_product_license($product_variation_id);

  // If no existing license, create one and add to cart.
  if (!$license) {

    // To CREATE, License must be added to Order Item AFTER
    // Order Item is added to cart, for originating order to be set.
    $cart_manager->addOrderItem($cart, $order_item);

    $uid = \Drupal::currentUser()->id();
    $license = $license_storage
      ->createFromProductVariation($product_variation, $uid);
    $license->set('state', 'pending');

    if ($order = $order_item->getOrder()) {

      $license->setOriginatingOrder($order);
    }
    $license->save();

    // Set the license field on the order item.
    $order_item->license = $license->id();
    $order_item->save();
  }
  // If existing license found.
  else {

    // To RENEW, License must be added to Order Item BEFORE
    // Order Item is added to cart, so license can adapt to new order.
    if ($license->canRenew()) {

      // Set the license field on the order item.
      $order_item->license = $license->id();
      $order_item->save();

      $cart_manager->addOrderItem($cart, $order_item);
    }
    else {

      // Warn user of renewal window start time.
      $renewal_window_start_time = $license->getRenewalWindowStartTime();

      /** @var \Drupal\Core\Messenger\Messenger $messenger */
      $messenger = \Drupal::service('messenger');

      /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
      $date_formatter = \Drupal::service('date.formatter');

      if (!is_null($renewal_window_start_time)) {

        $messenger->addStatus(t('You have an existing license. You will be able to renew your license after %date.', [
          '%date' => $date_formatter->format($renewal_window_start_time),
        ]));
      }
    }
  }

  return $license;
}

/**
 * Returns File items referenced by Media Entity.
 *
 * @param \Drupal\media\MediaInterface $media
 *   Media Entity.
 *
 * @return array
 *   Array of File items.
 */
function commerce_license_media_disk_quota_get_files($media) {

  // Get Media File sourcefield name.
  $source = $media->getSource();
  $sourcefield_name = $source->getSourceFieldDefinition(
    $media->bundle->entity
  )->getName();

  // Load all files from Media File sourcefield.
  $file_items = $media->get($sourcefield_name)->referencedEntities();

  return $file_items;
}

/**
 * Create a new 'automated_content_license' Product Variation.
 *
 * To avoid conflicts between multiple single page licenses set on
 * different nodes but added to the cart at the same time,
 *
 * "Single page publishing license" products are used as templates
 * and automatically added as "Automated single page user license"
 * Product Variations instead.
 *
 * @param int $template_product_variation_id
 *   Template product variation to duplicate.
 * @param bool $renew_license
 *   Renew license flag.
 *
 * @return array
 *   Field array.
 */
function commerce_license_media_disk_quota_create_product_variation(
  $template_product_variation_id,
  $renew_license
) {

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  // Get template Product Variation.
  /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variations */
  $template_product_variation = $entity_manager
    ->getStorage('commerce_product_variation')->load($template_product_variation_id);

  if (!empty($template_product_variation)) {

    // Get template Product Variation type.
    $template_pv_type = $template_product_variation->bundle();

    // If a media disk quota variation.
    if ($template_pv_type == 'media_disk_quota_license') {

      // Generate new Product Variation sku.
      $user_id = \Drupal::currentUser()->id();
      $sku = $template_product_variation->sku->value . "-$user_id";

      if ($renew_license == 1) {

        // Load existing product variation matching template, if one exists.
        $product_variation_ids = [];
        $query = $entity_manager
          ->getStorage('commerce_product_variation')->getQuery()
          ->accessCheck(TRUE)
          ->condition('type', 'automated_content_license')
          ->condition('field_template_product_variation.target_id',
            $template_product_variation_id);

        $product_variation_ids = $query->execute();

        $product_variation = '';
        if (!empty($product_variation_ids)) {

          $product_variation = $entity_manager
            ->getStorage('commerce_product_variation')
            ->load(array_keys($product_variation_ids)[0]);
        }
      }

      // If no existing product variation was found, create.
      if (empty($product_variation)) {

        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
        $product_variation = ProductVariation::create([
          'type' => 'automated_content_license',
          'status' => 1,
          'field_template_product_variation' => $template_product_variation,
        ]);

        $product_variation->save();
      }

      // Update product variation to match current template settings.
      $product_variation->sku->value = $sku;
      $product_variation->title = $template_product_variation->title;
      $product_variation->price = $template_product_variation->price;
      $product_variation->list_price = $template_product_variation->list_price;
      $product_variation->license_expiration =
        $template_product_variation->license_expiration;
      $product_variation->license_type = $template_product_variation->license_type;
      $product_variation->field_disk_quota =
      $template_product_variation->field_disk_quota;
      $product_variation->field_grace_period =
      $template_product_variation->field_grace_period;
      $product_variation->save();

      /** @var \Drupal\commerce_store\CurrentStoreInterface $current_store */
      $current_store = \Drupal::service('commerce_store.current_store');

      $store = $current_store->getStore();

      // Search for existing Publish Content automated user license Product.
      $query = $entity_manager->getStorage('commerce_product')->getQuery()
        ->accessCheck(TRUE)
        ->condition('type', 'automated_content_license')
        ->condition('title', 'Automated content license')
        ->condition('uid', 1);

      $product_query = $query->execute();

      // Retrieve or create Product.
      if (!empty($product_query)) {

        $product_id = key($product_query);

        /** @var \Drupal\Commerce_product\Entity\ProductInterface $product */
        $product = \Drupal::service('entity_type.manager')
          ->getStorage('commerce_product')->load($product_id);
      }

      // Create Product if none exists.
      if (empty($product)) {

        /** @var \Drupal\Commerce_product\Entity\ProductInterface $product */
        $product = Product::create([
          'uid' => 1,
          'type' => 'automated_content_license',
          'title' => 'Automated content license',
          'stores' => [$store],
          'variations' => [$product_variation],
        ]);
      }

      // Add Variation to Product and save.
      $product->addVariation($product_variation);
      $product->save();

      return $product_variation;
    }
    else {

      // If not a single page license Product Variation, return template.
      return $template_product_variation;
    }
  }

  return FALSE;
}

/**
 * Formats a bytes integer value into a human readable representation.
 *
 * @param int $bytes
 *   Byte size integer.
 *
 * @return string
 *   Human readable byte size string.
 */
function commerce_license_media_disk_quota_format_filesize($bytes) {
  $labels = ['B', 'KB', 'MB', 'GB', 'TB'];

  foreach ($labels as $label) {
    if ($bytes > 1024) {
      $bytes = $bytes / 1024;
    }
    else {
      break;
    }
  }

  return round($bytes, 2) . $label;
}

/**
 * Returns mapped fields on a given Media Type.
 *
 * @param string $bundle
 *   Bundle of media type.
 *
 * @return string
 *   Array mapped fields.
 */
function commerce_license_media_disk_quota_field_map($bundle) {

  $entity_manager = \Drupal::service('entity_type.manager');
  $media_type_storage = $entity_manager->getStorage('media_type');
  $media_type = $media_type_storage->load($bundle);
  $fieldmap = $media_type->get('field_map');

  return $fieldmap;
}

/**
 * Updates the target entity field on the Automated Content License Product.
 *
 * Updates the 'field_licensed_entity' target_bundles to all current node types.
 */
function commerce_license_media_disk_quota_update_media_license_target_field() {

  $media_bundles = \Drupal::service('entity_type.bundle.info')
    ->getBundleInfo('media');
  foreach (array_keys($media_bundles) as $media_bundle) {

    // Load field.
    $field = FieldConfig::loadByName('media',
      $media_bundle, 'field_media_disk_quota_license');

    if ($field) {

      // Ensure field is set to target current license bundles.
      $settings = [
        'handler' => 'default:commerce_license',
        'handler_settings' => [
          'target_bundles' => [
            'media_disk_quota' => 'media_disk_quota',
            'publish_membership' => 'publish_membership',
          ],
          'sort' => [
            'field' => '_none',
            'direction' => 'ASC',
          ],
          'auto_create' => FALSE,
          'auto_create_bundle' => 'media_disk_quota',
        ],
      ];

      $field->set('settings', $settings);
      $field->save();
    }
  }
}
