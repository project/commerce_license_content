<?php

/**
 * @file
 * Provides installation hooks for the Commerce License Media Disk Quota.
 */

use Drupal\commerce_product\Entity\Product;

/**
 * Implements hook_install().
 */
function commerce_license_media_disk_quota_install() {

  /** @var \Drupal\commerce_store\CurrentStoreInterface $current_store */
  $current_store = \Drupal::service('commerce_store.current_store');
  $store = $current_store->getStore();

  // Create Commerce License Publish Content Products.
  if ($store) {

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
    $entity_manager = \Drupal::service('entity_type.manager');

    $license_products = [
      'media_disk_quota_license' => 'Media storage license',
    ];

    foreach ($license_products as $license_product => $label) {

      // Search for existing Publish Content automated user license Product.
      $query = $entity_manager->getStorage('commerce_product')->getQuery()
        ->accessCheck(TRUE)
        ->condition('type', $license_product);

      $product_query = $query->execute();

      // Create Product if none exists.
      if (empty($product_query)) {

        /** @var \Drupal\Commerce_product\Entity\ProductInterface $product */
        $product = Product::create([
          'uid' => 1,
          'type' => $license_product,
          'title' => $label,
          'stores' => [$store],
        ]);
        $product->save();
      }
    }
  }

  // Update Automated Content+Single License Product Variations
  // to target all Licensed node types.
  commerce_license_content_update_product_variation_target_field();

  // Update license field target_id settings on Media.
  commerce_license_media_disk_quota_update_media_license_target_field();
}

/**
 * Implements hook_pre_uninstall().
 */
function commerce_license_media_disk_quota_uninstall() {

  // Delete View.
  \Drupal::service('config.factory')
    ->getEditable('views.view.commerce_license_media_disk_quota')->delete();

  // Delete bundle state keys and Field.
  $bundles = \Drupal::service('entity_type.bundle.info')
    ->getBundleInfo('media');

  foreach (array_keys($bundles) as $bundle) {

    commerce_license_content_remove_field(
      'media', $bundle, 'field_media_disk_quota_license');

    $settings =
      ['products', 'order_field', 'grace_period', 'delete_action'];

    foreach ($settings as $setting) {

      $key = 'commerce_license_media_disk_quota_' . $setting . '_' . $bundle;
      \Drupal::state()->delete($key);
    }
  }

  // Delete entities and entity types.
  $entity_list = [
    'commerce_product_variation' => ['type' => 'media_disk_quota_license'],
    'commerce_product' => ['type' => 'media_disk_quota_license'],
    'commerce_product_variation_type' => ['id' => 'media_disk_quota_license'],
    'commerce_product_type' => ['id' => 'media_disk_quota_license'],
  ];
  commerce_license_content_remove_entities($entity_list);

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager */
  $entity_manager = \Drupal::service('entity_type.manager');

  // Delete queue.
  $queue_storage = $entity_manager->getStorage('advancedqueue_queue');

  /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
  $queue = $queue_storage->load('commerce_license_media_disk_quota');

  if ($queue) {

    $queue->delete();
  }
}
