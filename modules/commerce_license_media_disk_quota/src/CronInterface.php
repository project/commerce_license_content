<?php

namespace Drupal\commerce_license_media_disk_quota;

/**
 * Provides the interface for the Media Disk Quota module's cron.
 *
 * Queues licensed Media for expiration.
 */
interface CronInterface {

  /**
   * Runs the cron.
   */
  public function run();

}
