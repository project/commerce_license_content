<?php

namespace Drupal\commerce_license_media_disk_quota\Plugin\Commerce\LicenseType;

use Drupal\commerce_license\Entity\LicenseInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\LicenseTypeBase;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides license for Media file user disk quota control.
 *
 * @CommerceLicenseType(
 *   id = "media_disk_quota",
 *   label = @Translation("Media storage quota"),
 * )
 */
class MediaDiskQuota extends LicenseTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user data manager service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The bytes utility.
   *
   * @var Drupal\Component\Utility\Bytes
   */
  protected $bytes;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    UserDataInterface $user_data,
    DateFormatterInterface $date_formatter,
    StateInterface $state,
    LoggerChannelFactoryInterface $logger_factory
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->userData = $user_data;
    $this->dateFormatter = $date_formatter;
    $this->state = $state;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('user.data'),
      $container->get('date.formatter'),
      $container->get('state'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildLabel(LicenseInterface $license) {

    $product_variation = $license->getPurchasedEntity();
    $product_label = $product_variation->label();

    $quota = $this->getLicenseQuotaData($license);
    // $amount = $quota['amount'];
    // $used = $quota['used'];
    $free = $quota['free'];
    // $used_percent = $quota['used_percent'];
    $free_percent = $quota['free_percent'];
    $media_count = $quota['target_entity_count'];

    if ($media_count == 1) {
      $media_count = "1 file";
    }
    else {
      $media_count = "$media_count files";
    }

    $label = "$product_label: $free_percent% free, $media_count, $free remaining";

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function grantLicense(LicenseInterface $license) {

    $owner = $license->getOwner();

    $user_quota = $this->getLicenseOwnerDiskQuota($license);
    $user_raw = Bytes::toNumber($user_quota);

    $license_quota = $this->getLicenseQuotaData($license);
    $license_raw = $license_quota['amount_raw'];

    // Add the two.
    $new_quota_amount = $user_raw + $license_raw;

    // Protect against unusual errors and states.
    if ($new_quota_amount < $user_raw) {

      $new_quota_amount = $user_raw;
    }

    // Set the new user disk quota amount.
    $uid = $owner->id();

    $this->userData->set('user_disk_quota', $uid, 'limit', $new_quota_amount);

    // Fetch the licensed media entities and set to published.
    $target_media = $this->fetchMediaTargetEntities($license);

    foreach ($target_media as $media) {
      $media->setPublished();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLicense(LicenseInterface $license) {

    $user_quota = $this->getLicenseOwnerDiskQuota($license);
    $user_raw = Bytes::toNumber($user_quota);

    $license_quota = $this->getLicenseQuotaData($license);
    $license_raw = $license_quota['amount_raw'];

    // Subtract the two.
    $new_quota_amount = $user_raw - $license_raw;

    // Protect against unusual errors and states.
    if ($new_quota_amount < $user_raw) {

      $new_quota_amount = $user_raw;
    }

    // Set the new user disk quota amount.
    $account = $license->getOwner();
    $uid = $account->id();

    $this->userData->set('user_disk_quota', $uid, 'limit', $new_quota_amount);

    // Fetch the licensed entities and set to unpublished.
    $media_list = $this->fetchMediaTargetEntities($license);

    foreach ($media_list as $media) {

      $media->setUnpublished();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    return $form;
  }

  /**
   * Get license owner quota limit.
   *
   * @param \Drupal\commerce_license\Entity\LicenseInterface $license
   *   The license to get the owner quota for.
   *
   * @return int
   *   The user quota limit, in Bytes.
   */
  public function getLicenseOwnerDiskQuota(LicenseInterface $license) {

    $account = $license->getOwner();

    // See user_disk_quota.module.
    $user_quota = _user_disk_quota_get_user_role_max_quota($account);

    return $user_quota;
  }

  /**
   * Returns an array of license disk quota statistics.
   *
   * @param \Drupal\commerce_license\Entity\LicenseInterface $license
   *   The license entity to return quota statistics on.
   *
   * @return array
   *   An array of license disk quota statistics.
   */
  public function getLicenseQuotaData(LicenseInterface $license) {

    $license_quota = 0;
    $license_quota_raw = 0;
    $product_variation = $license->product_variation->entity;
    if ($product_variation instanceof ProductVariation) {

      if (isset($product_variation->field_disk_quota->value)) {

        $license_quota = $product_variation->field_disk_quota->value;
        $license_quota_raw = Bytes::toNumber($license_quota);
      }
    }

    $used_raw = 0;
    $target_entity_count = 0;

    $target_entities = $this->fetchMediaTargetEntities($license);
    foreach ($target_entities as $target_entity) {

      $target_entity_count++;

      // Get size field mapping from Media bundle.
      $bundle = $target_entity->bundle();
      $media_type = $this->entityTypeManager
        ->getStorage('media_type')->load($bundle);
      $field_map = $media_type->getFieldMap();
      $filesize_field = $field_map['filesize'];

      if ($filesize_field) {

        $media_filesize = $target_entity->get($filesize_field)->value;
        $used_raw = $used_raw + $media_filesize;
      }
      else {

        $this->loggerFactory->get('commerce_license_media_disk_quota')
          ->error('Filesize field not defined for %bundle Media Type. Unable to process Media Disk Quota license. See installation step 4 in README.md.', ['%bundle' => $bundle]);
      }
    }

    $used = $this->formatFilesize($used_raw);

    $free_raw = 0;
    $free = '0MB';
    $used_percentage = 0;
    $free_percentage = 0;
    if ($license_quota_raw > 0 && $license_quota_raw > $used_raw) {

      $free_raw = $license_quota_raw - $used_raw;
      $free = $this->formatFilesize($free_raw);

      $used_percentage = round($used_raw / $free_raw * 100, 2);
      $free_percentage = round(100 - $used_percentage, 2);
    }

    $quota = [
      'target_entity_count' => $target_entity_count,
      'amount' => $license_quota,
      'amount_raw' => $license_quota_raw,
      'used' => $used,
      'used_raw' => $used_raw,
      'free' => $free,
      'free_raw' => $free_raw,
      'used_percent' => $used_percentage,
      'free_percent' => $free_percentage,
    ];

    return $quota;
  }

  /**
   * Fetches the target entities to be controlled by the license.
   */
  public function fetchMediaTargetEntities(LicenseInterface $license) {

    $media_targets = [];
    $license_id = $license->id();

    // Get the order item matching this license.
    $query = $this->entityTypeManager->getStorage('media')->getQuery()
      ->accessCheck(FALSE)
      ->condition('field_media_disk_quota_license', $license_id);

    $results = $query->execute();

    foreach ($results as $mid) {
      $media_targets[$mid] = $this->entityTypeManager->getStorage('media')
        ->load($mid);
    }

    return $media_targets;
  }

  /**
   * Formats bytes to a human readable representation.
   *
   * @param int $bytes
   *   Integer bytes to be converted to human readable text;.
   *
   * @return string
   *   Value in human readable byte notation.
   */
  private function formatFilesize($bytes) {
    $labels = ['B', 'KB', 'MB', 'GB', 'TB'];

    foreach ($labels as $label) {
      if ($bytes > 1024) {
        $bytes = $bytes / 1024;
      }
      else {
        break;
      }
    }

    return round($bytes, 2) . $label;
  }

}
