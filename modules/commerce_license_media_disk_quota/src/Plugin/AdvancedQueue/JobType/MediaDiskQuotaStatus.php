<?php

namespace Drupal\commerce_license_media_disk_quota\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the job type for publishing/unpublishing licensed Media.
 *
 * @AdvancedQueueJobType(
 *   id = "commerce_license_media_disk_quota_status",
 *   label = @Translation("Change publish status of licensed media"),
 * )
 */
class MediaDiskQuotaStatus extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new MediaDiskQuotaExpire object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {

    $media_id = $job->getPayload()['media_id'];
    $media_storage = $this->entityTypeManager->getStorage('media');

    /** @var \Drupal\media\Entity\Media $media */
    $media = $media_storage->load($media_id);

    if (!$media) {
      return JobResult::failure('Media not found.');
    }

    if (isset($media->field_media_disk_quota_license->entity)) {

      $license = $media->field_media_disk_quota_license->entity;
      $license_state = $license->getState()->value;

      if (
        $license_state == 'active'
        || $license_state == 'renewal_in_progress'
      ) {

        $expires_time = $license->getExpiresTime();
        $now = $this->time->getRequestTime();

        if ($expires_time < $now) {

          $media->setPublished();
          $media->save();

          return JobResult::success("Status changed to published.");
        }
      }
    }

    $media->setUnpublished();
    $media->save();

    return JobResult::success("Status changed to unpublished.");
  }

}
