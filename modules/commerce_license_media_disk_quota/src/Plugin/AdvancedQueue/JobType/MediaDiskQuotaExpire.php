<?php

namespace Drupal\commerce_license_media_disk_quota\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the job type for expiring unlicensed Media.
 *
 * @AdvancedQueueJobType(
 *   id = "commerce_license_media_disk_quota_expire",
 *   label = @Translation("Expire unlicensed media"),
 * )
 */
class MediaDiskQuotaExpire extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new MediaDiskQuotaExpire object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {

    $media_id = $job->getPayload()['media_id'];
    $media_storage = $this->entityTypeManager->getStorage('media');

    /** @var \Drupal\media\Entity\Media $media */
    $media = $media_storage->load($media_id);

    if (!$media) {
      return JobResult::failure('Media not found.');
    }

    $media_owner = $media->getOwnerId();
    if ($media_owner == 1) {
      return JobResult::failure('Media belongs to administrator.');
    }

    if (isset($media->field_media_disk_quota->license->entity)) {

      $license = $media->field_media_disk_quota_license->entity;

      $license_state = $license->getState()->value;

      if ($license_state == 'active' || $license_state == 'renewal_in_progress') {

        $expires_time = $license->getExpiresTime();
        $now = $this->time->getRequestTime();

        if ($expires_time < $now) {

          return JobResult::failure('Media license is active.');
        }
      }
    }

    $bundle = $media->bundle();

    // Fetch delete action set on bundle.
    $key = 'commerce_license_media_disk_quota_delete_action_' . $bundle;
    $delete_action = $this->state->get($key, []);

    if ($delete_action == 'delete') {

      $media->delete();
      return JobResult::success();
    }

    if ($delete_action == 'change_uid') {

      $this->setMediaOwnerId($media);
      return JobResult::success();
    }

    if ($delete_action == 'unpublish') {

      $media->setUnpublished();
      $media->save();
      return JobResult::success();
    }

    return JobResult::failure("Invalid delete action '$delete_action' set on unlicensed '$bundle' media. No action taken.");

  }

  /**
   * Changes ownership of a Media Entity and the Files it references.
   *
   * @param object $media
   *   The media entity to change ownership.
   * @param int $uid
   *   Optional user id to change ownership to. Defaults to administrator.
   */
  private function setMediaOwnerId($media, $uid = 1) {

    // Get Media Entity sourcefield name.
    $source = $media->getSource();
    $sourcefield_name = $source->getSourceFieldDefinition(
      $media->bundle->entity
    )->getName();

    // Load all Files entities from Media Entity sourcefield.
    $file_items = $media->get($sourcefield_name)->referencedEntities();

    // Change File Entity ownership.
    foreach ($file_items as $file_item) {
      $file_item->setOwnerId(1);
      $file_item->save();
    }

    // Clear license field.
    $media->set('field_media_disk_quota_license', '');

    // Unpublish Media.
    $media->setUnpublished();

    // Change Media ownership to administrator.
    $media->setOwnerId(1);
    $media->save();
  }

}
