<?php

namespace Drupal\commerce_license_media_disk_quota;

use Drupal\advancedqueue\Job;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\State\StateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Default cron implementation.
 */
class Cron implements CronInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityFieldManagerInterface $entity_field_manager,
    TimeInterface $time,
    StateInterface $state
  ) {

    // parent::__construct($configuration, $plugin_id, $plugin_definition);.
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function run() {

    $queue_storage = $this->entityTypeManager->getStorage('advancedqueue_queue');

    /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
    $queue = $queue_storage->load('commerce_license_media_disk_quota');

    $now = $this->time->getRequestTime();

    $unpublish_media_ids = $this->getLicensedMediaToUnpublish($now);
    if ($unpublish_media_ids) {

      foreach ($unpublish_media_ids as $media_id) {

        // Create a job and queue each one up.
        $unpublish_media_job =
          Job::create('commerce_license_media_disk_quota_status',
            ['media_id' => $media_id]);

        $queue->enqueueJob($unpublish_media_job);
      }
    }

    $expired_media_ids = $this->getLicensedMediaToExpire($now);
    if ($expired_media_ids) {

      foreach ($expired_media_ids as $media_id) {

        // Create a job and queue each one up.
        $expire_media_job =
          Job::create('commerce_license_media_disk_quota_expire',
            ['media_id' => $media_id]);

        $queue->enqueueJob($expire_media_job);
      }
    }
  }

  /**
   * Gets IDs of unlicensed Media.
   *
   * @param int $time
   *   Time to check against license expiration.
   *
   * @return array
   *   IDs of matching commerce_license entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLicensedMediaToUnpublish(int $time): array {

    // Get a list of Media Type bundles.
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('media');
    $bundle_list = array_keys($bundle_info);

    // Get a list of the Media Type bundles with a license field.
    $unlicensed_media_ids = [];
    foreach ($bundle_list as $bundle) {

      // Check for License field.
      $field = FieldConfig::loadByName('media', $bundle,
        'field_media_disk_quota_license');

      if ($field) {

        // Fetch all published Media without a license.
        $query = $this->entityTypeManager->getStorage('media')
          ->getQuery()
          ->accessCheck(FALSE)
          ->condition('bundle', $bundle)
          ->condition('status', 1);

        $orCondition = $query->orConditionGroup();
        $orCondition->notExists('field_media_disk_quota_license');
        $orCondition->condition('field_media_disk_quota_license.entity.state',
            'active', 'NOT IN');
        $query->condition($orCondition);

        $unlicensed_bundle_media_ids = $query->execute();
        $unlicensed_media_ids += $unlicensed_bundle_media_ids;
      }
    }

    return $unlicensed_media_ids;
  }

  /**
   * Gets IDs of licensed Media that is set to expire.
   *
   * @param int $time
   *   Time to check against license expiration.
   *
   * @return array
   *   IDs of matching commerce_license entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLicensedMediaToExpire(int $time): array {

    // Get a list of Media Type bundles.
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('media');
    $bundle_list = array_keys($bundle_info);

    // Get a list of the Media Type bundles with a license field.
    $expired_media_ids = [];
    foreach ($bundle_list as $bundle) {

      // Check for License field.
      $field = FieldConfig::loadByName('media', $bundle,
        'field_media_disk_quota_license');

      if ($field) {

        // Get delete action setting for bundle.
        $key = 'commerce_license_media_disk_quota_delete_action_' . $bundle;
        $delete_action = $this->state->get($key, 'unpublish');

        if ($delete_action != 'unpublish') {

          // Get grace period setting for bundle.
          $key = 'commerce_license_media_disk_quota_grace_period_' . $bundle;
          $grace_period = $this->state->get($key, 0);

          // Calculate expire time.
          $grace_seconds = strtotime(
            $grace_period['interval'] . " " . $grace_period['period']
          );
          $expire_time = $time - $grace_seconds;

          // Fetch all the non-active license expired Media ids.
          // Exclude administrator Media.
          $query = $this->entityTypeManager->getStorage('media')
            ->getQuery()
            ->accessCheck(FALSE)
            ->condition('bundle', $bundle)
            ->condition('changed', $expire_time, '>=')
            ->condition('uid', 1, '!=');

          $orCondition = $query->orConditionGroup();
          $orCondition->notExists('field_media_disk_quota_license');
          $orCondition->condition('field_media_disk_quota_license.entity.state',
            'active', 'NOT IN');
          $query->condition($orCondition);

          $expired_bundle_media_ids = $query->execute();
          $expired_media_ids += $expired_bundle_media_ids;
        }
      }
    }

    return $expired_media_ids;
  }

}
