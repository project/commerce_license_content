# Commerce License Media Disk Quota Documentation

## About

This module provides a Commerce License Type plugin
for the licensed management of disk quota on Media file storage.

Includes Products, Views, Order and License status Fields
for configured Media Types.


## Table of contents

- Recommended modules
- Installation
- Configuration
- FAQ
- Maintainers
- Sponsors
- Donations


## Recommended modules

[Markdown filter](https://www.drupal.org/project/markdown): When enabled,
display of the project's README.md help will be rendered with markdown.


## Installation

Install the User Disk Quota module:
    composer require drupal/user_disk_quota:1.x-dev 

For User Disk Quota version 8.x-1.x-dev, patch the bug.
See: https://www.drupal.org/project/user_disk_quota/issues/3284164

Enable Commerce License Media Disk Quota:
    drush en commerce_license_media_disk_quota


## Configuration

1. Create a default Commerce Store if you haven't already.
   See: /store/add.

2. Create a Commerce Payment Method if you haven't already.
   See: /admin/commerce/config/payment-gateways.

3. Set the Allow renewal window on License Product Variation Types.
   See: /admin/commerce/config/product-variation-types.

   Configure renewal window on Media disk quota license.
   This setting allows a customer to renew their license by
   re-purchasing the product. Defaults to "1 Weeks".

4. Configure License Product.
   See: /admin/commerce/products.

   - Edit the 'Media disk quota license' Product.
   - Change the title if desired.
   - Ensure your Product is published.
   - Consider setting a URL alias for direct purchase.

   - Click "Save and add variations".
   - Click "Add variation":

   - Choose a title. Short titles describing the disk quota and the 
     license interval are recommended.
   e.g. "1 week 1GB storage".

   - Choose an SKU.
   **Note: These Product Variations are sorted alpha-numerically by SKU
   on Order Form Fields.**

   - Configure your disk quota amount, e.g "1GB".
   - Ensure the License Type is set to "Media disk storage quota".

   - Configure the license expiration.
   e.g. Rolling interval: 1 Weeks.

   - Ensure Published is checked.
   - Click "Save".

5. Configure Media Types for License management:
   See: /admin/structure/media.

	 An example Media Type called "Licensed Video" has been provided.

   - Click Edit on the Media Content type you want to license.
   i.e. Licensed video.
   - Check that the File size Field mapping is set.

   **The File size field must be mapped for
   Media Disk Quota Licenses to work.**

   If not, go to Manage fields and re-use the existing "Filesize"
   field provided. Or add a Text (plain) field for this purpose.

   Go back to Edit, and set the File size Field mapping to your new field.

   - Configure the **License settings** vertical tab.
	 (This tab appears once License Product Variations are available.)

   - Select the **License options** (Product Variations) you want
		 available for purchase on this Media type.

   **Warning: enabling these options will activate the License field
   on this Media Type.**
   Remove the License field to restore to normal.
   See: /admin/commerce/config/commerce_license_content_delete

   - Check **Activate order form field** to add a License Order Form
     Field to Node edit forms of this Content Type.
     (You might opt to only sell Unlimited Licenses directly
     from the Product view.)

	 - Set the Grace period and Expiry action.

   - Click Manage form display.
     Ensure the "License field" Widget is visible and
     set to "Select list".
   - Click on Save.

5. Set complimentary Disk Quota amounts on your Roles.
   See: /admin/config/people/accounts/disk-quota

   e.g. Authenticated user's disk quota limit: 1GB.
	 Adding a complimentary quota will improve the user experience
	 by activating the inline order form field, and allowing temporary
   files to be uploaded during purchase of License.

   **Leave blank to disable quota limits.**
   So at the minimum, you probably want to set a value for the
   authenticated Role.

	 Note: When Users max their quota, Media entities cannot be saved,
   which renders the order form Field inoperative.
   In this situation the Field is replaced with a link to the 
   Product View, for ease of navigation to purchase.
   See: /order-storage.

6. Configure permissions for your Roles:
   See: /admin/people/permissions.

   - Media:
    - e.g. Licensed video: Create/Edit/Delete new content.
    - Access media overview.
    - **View own unpublished media** (important).

Your site is now ready to serve licensed media.
See: /media/add.


## FAQ

**Q: How do Licenses work with ithe User Disk Quota module?**

**A:** All of the magic happens via the **Quota limit** User account Field.

- When a License is actived, the purchased quota amount is added to this field.
- When a License expires, it is deducted from the total in this field.
**So it is best never to edit this field.**

However, you can still modify this amount as desired.
e.g. By upping the total for special Users.
This will override the balance as managed by the licenses and set a new
balance. If you reduce this value, your customer will not
get all the quota they have paid for.

**Q: Why are my license options not appearing in media edit forms.**

New fields will appear on media edit forms and displays showing relevant license
and order status details, once Product options are created and added to the
Media Type settings. See configuration step 4.
 

**Q: Why are my licenses not activating or expiring.**

**A:** Licenses are automatically activated upon full payment of the
associated Commerce Order. You must ensure cron is correctly configured
for this to occur.

Troubleshooting: Orphaned licenses lost in the database tables can prevent the
queue from activating and expiring licenses.
As can code errors caused by other modules.

Job status information can be viewed in the Queues.
See: /admin/config/system/queues.


**Q: How are licenses purchased.**

**A:**
- A View for purchasing storage licenses.
  See: /order-storage.
- Product Variations can be ordered directly from Media Type edit forms, once
  a Role quota is set. See step 5.
	Media entities cannot be saved once quota is maxed.


**Q: What views are provided.**

**A:**
- Alternative cart and checkout views are included
  with removed product quantity fields and disk quota fields added.
- An example "Purchase disk quota license" Product view.
  (Go to: /purchase-media-disk-quota)

**Q: What files can be controlled by disk quotas.**

**A:**
Only files uploaded within Media Types are managed by Media Disk Quota.
Field Types that access the filesystem directly, such as "Image"
and "File" fields, are not managed.


## Maintainers 

- Dan Greenman - Freelance developer for hire.
  [dangreenman] (https://www.drupal.org/u/dangreenman).

Commission your wildest Open Source software dreams.
See: [Portfolio] (https://greenman-it.pw)


## Sponsors

[Metro Rentals] (http://www.metro-rentals.com/)

Thank you for your kind donation.


## Donations

If you value this module and would like to show your appreciation and support,
please consider sending a donation here:

- [Monero] (44KTVNFzSmC12srxKgxvCEbmUXSzjNmT1NAHRpF9tuhvCDMpsimTZerAxPr4pNrtT7EjqN45WKerrAh1K9UgKayR9ogksYm)

Thank you to the Drupal community for all your fantastic work.
