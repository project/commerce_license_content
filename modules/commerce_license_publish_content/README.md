# Commerce License Publish Content Documentation

## About

This module provides a Commerce License Type plugin
for the licensed management of audience access to content.

Includes Products, Views, Order and license status Fields
for configured Content Types.


## Table of contents

- Recommended modules
- Installation
- Configuration
- FAQ
- Maintainers
- Donations


## Recommended modules

[Markdown filter](https://www.drupal.org/project/markdown): When enabled,
display of the project's README.md help will be rendered with markdown.


## Installation

Enable Commerce License Publish Content:
    drush en commerce_license_publish_content


## Configuration

1. Create a default Commerce Store if you haven't already.
   See: /store/add.

2. Create a Commerce Payment Gateway if you haven't already.
   See: /admin/commerce/config/payment-gateways.

3. Set the Allow renewal window on license Product Variation Types.
   See: /admin/commerce/config/product-variation-types.

   Configure renewal window on:
   - Publish single page license.
   - Publish unlimited pages license.

   This setting allows a customer to renew their license by
   re-purchasing the product. Defaults to "1 Weeks".

4. Configure License Products.
   See: /admin/commerce/products.

   - Edit the 'Publish single page license' and
     'Unlimited pages publishing' Products.
   - Change the title if desired.
   - Ensure your Products are published.
   - On the "Unlimited pages publishing license" Product,
     consider setting a URL alias and menu link, to enable License
     purchases from the Product directly.

   **Single page licenses can only be purchased directly from the
   Order Form Field on the Node edit form.**

   - Click "Save and add variations".
   - Click "Add variation":

   - Choose a Title. Short titles describing the license interval and the 
     license type are recommended.
   e.g. "1 week single page" or "1 week unlimited pages".

   - Choose an SKU.
   **Note: Product Variations are list sorted alpha-numerically by SKU
   on Order Form Fields.**

   - Ensure the correct License Type is set.
   e.g. "Publish single page" on "Publish single page license" Products,
   and "Publish unlimited pages" on "Unlimited pages publishing license"
   Products.

   - Configure the license expiration.
   e.g. Rolling interval: 1 Weeks.

   - Ensure Published is checked.
   - Click "Save".

5. Configure Content Types for License management:
   See: /admin/structure/types.

   - Click Edit on a Content Type you wish to License. i.e. Article.
   - Configure the **License settings** vertical tab.
   (This tab appears once License Product Variations are available.)

   - Select the **License options** (Product Variations) you want
     available for purchase on this Content Type.

   **Warning: enabling these options will activate the License field
   on this Content Type.**
   Remove the License field to restore to normal.
   See: /admin/commerce/config/commerce_license_content_delete

   - Check **Activate order form field** to add a License Order Form
     Field to Node edit forms of this Content Type.
     (You might opt to only sell Unlimited Licenses directly
     from the Product view.)

   - Check **Control node published status** if you would like to disable 
     the publish status checkbox on the node edit forms, and have this
     setting be automatically controlled by the License Field instead.

     For scenarios requiring licensed Content without access control,
     uncheck:
    - Control node published status.
    - Enable access control restrictions on licensed content.

   **Warning: this will enable everybody to view unlicensed content.**
   As per normal content permissions.

   - For sites that host large numbers of nodes per license, select
   **Use the queue manager** to manage the node publishing process via
   queued cron.
     Ensure your cron settings are well adjusted for timely publication.
     Note: content status (publishing) updates occur in the queue one cron
     cycle behind license state change.

   - Click Manage form display.
     Ensure the "License field" Widget is visible and
     set to "Select list".
   - Click on Save.

6. Configure permissions on your Roles:
   See: /admin/people/permissions.

   - Set Commerce License Publish Content: View unlicensed content
     for moderator roles to be able to view unlicensed content.
   - Node: 
    - e.g. Article: Create/Edit/Delete new content.
    - Access the Content overview.
    - **View own unpublished content** (important).

Your site is now ready to serve licensed content.
See: /node/add.


## FAQ

**Q: Why are my license options not appearing in the node edit form.**

New fields will appear on node edit forms and displays showing relevant license
and order status details, once Product options are created and added to the
Content Type settings. See configuration steps 4 & 5.


**Q: Why are my licenses not activating or expiring.**

**A:** Licenses are automatically activated upon full payment of the
associated Commerce Order. You must ensure cron is correctly configured
for this to occur.

Troubleshooting: Orphaned licenses lost in the database tables can prevent the
queue from activating and expiring licenses. Code errors from other modules
can also halt this process.

Job status information can be viewed in the Queues.
See: /admin/config/system/queues.


**Q: How are licenses purchased.**

**A:**
- License Product Variations can be ordered from the License Field
  on Node edit forms.
- Unlimited pages licenses can be purchased directly from the Product view.
  See step 4: URL alias.

**Q: What views are provided.**

**A:**
- Commerce License Publish Content:
  A View display unlimited license status information with
  a summary of managed pages.

**Q: What is the Automated content license Product?**

**A:**
"Single page publishing license" products are used as templates
and are automatically saved as a "Automated content license" Product
Variations instead, to avoid conflicts and enable multiple single page
licenses for different content to be added to the Cart at the same time.

**Do not create, edit or delete this Product or it might break.**

## Maintainers 


- Dan Greenman - Freelance developer for hire.
  [dangreenman] (https://www.drupal.org/u/dangreenman).

Commission your wildest Open Source software dreams.
See: [Portfolio] (https://greenman-it.pw)


## Sponsors

[Metro Rentals] (http://www.metro-rentals.com/)

Thank you for your kind donation.


## Donations

If you value this module and would like to show your appreciation and support,
please consider sending a donation here:

- [Monero] (44KTVNFzSmC12srxKgxvCEbmUXSzjNmT1NAHRpF9tuhvCDMpsimTZerAxPr4pNrtT7EjqN45WKerrAh1K9UgKayR9ogksYm)

Thank you to the Drupal community for all your fantastic work.
