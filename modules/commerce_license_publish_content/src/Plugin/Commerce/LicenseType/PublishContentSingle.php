<?php

namespace Drupal\commerce_license_publish_content\Plugin\Commerce\LicenseType;

use Drupal\commerce_license\Entity\LicenseInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\LicenseTypeBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a license type for audience access control on target entities.
 *
 * @CommerceLicenseType(
 *  id = "publish_single",
 *  label = @Translation("Publish single page"),
 * )
 */
class PublishContentSingle extends LicenseTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger_factory
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildLabel(LicenseInterface $license) {

    $purchased_entity = $license->getPurchasedEntity();

    // Protect against deleted product variations.
    if ($purchased_entity) {

      $title = $purchased_entity->label();
      $label = "$title";
    }
    else {

      $license_id = $license->id();
      $this->loggerFactory->get('commerce_license_publish_content')
        ->error('Product variation for License id @license_id is missing. Deleting license.', ['@license_id' => $license_id]);

      $license->delete();
      $label = 'deleted';
    }

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function grantLicense(LicenseInterface $license) {

    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLicense(LicenseInterface $license) {

    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    return $form;
  }

}
