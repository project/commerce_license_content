# Commerce License Membership Content Documentation

## About

This module provides a Commerce License Type plugin
for the licensed management of membership roles, audience access to
content and disk quota on Media file storage.

Includes Products, Views and License status Fields
for configured Media Types.


## Table of contents

- Recommended modules
- Installation
- Configuration
- Maintainers
- Donations


## Recommended modules

[Markdown filter](https://www.drupal.org/project/markdown): When enabled,
display of the project's README.md help will be rendered with markdown.


## Installation

As per the README.md instructions provided with each sub module:
- Install the Commerce License Publish Content module.
- Install the Commerce License Media Disk Quota module.

Enable Commerce License Membership Content:
    drush en commerce_license_membership_content


## Configuration

1. Create a new role for your members. i.e. Gold members.

2. Configure License Product (Go to: /product/add):

   - Edit the 'Membership content license' Product.
   - Change the title if desired.
   - Ensure your Product is published.

   - Click "Save and add variations".
   - Click "Add variation":

   - Choose a title. Short titles clearly describing the role, disk quota
     and license interval are recommended,
   e.g. "Gold member: unlimited publishing with 10GB storage for 1 year".

   - Choose an SKU.
   **Note: These Product Variations are sorted alpha-numerically by SKU
   on Order Form Fields.**

   - Configure your disk quota amount, e.g "10 GB".
   - Ensure the License Type is set to "Membership content license".

   - Configure the license expiration.
    e.g. Rolling interval: 1 Years.

   - Ensure Published is checked.
   - Click "Save".

3. As per the README.md instructions provided in the sub modules:
  - Add your new Product Variations to the Content and Media Type
    License options.
  - Set complimentary Disk Quota amounts for your Roles.
  - Configure permissions on your Roles.

Your site is now ready to serve licensed membership content.
See: /node/add.
See: /media/add.


## Maintainers 

- Dan Greenman - Freelance developer for hire.
  [dangreenman] (https://www.drupal.org/u/dangreenman).

Commission your wildest Open Source software dreams.
See: [Portfolio] (https://greenman-it.pw)


## Sponsors

[Metro Rentals] (http://www.metro-rentals.com/)

Thank you for your kind donation.


## Donations

If you value this module and would like to show your appreciation and support,
please consider sending a donation here:

- [Monero] (44KTVNFzSmC12srxKgxvCEbmUXSzjNmT1NAHRpF9tuhvCDMpsimTZerAxPr4pNrtT7EjqN45WKerrAh1K9UgKayR9ogksYm)

Thank you to the Drupal community for all your fantastic work.
